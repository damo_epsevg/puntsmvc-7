package edu.upc.damo.punts_mvc;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

public class MainActivity extends Activity {

    static final String TAG ="PROVA";

    private EditText t1;
    private EditText t2;

    private CjtDePunts cjtDePunts = new CjtDePunts();       // El Model
    private VistaDePunts vista;                          // La vista

    private VistaPunt vistaPunt; // Vista d'un punt

    private Random generador = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();

         vista.defineixModel(cjtDePunts);
         vistaPunt =new VistaPunt(t1,t2);
    }


    /* Inicialització de l'activity */

    private void inicialitza() {
        t1 = (EditText) findViewById(R.id.text1);
        t2 = (EditText) findViewById(R.id.text2);
        vista = (VistaDePunts) findViewById(R.id.vistaDePunts);


        // En aquest moment la view encara no existeix, i per tant no té mesures.
        // De moment creem el generador, i més endavant ja li indicarem els límits

        findViewById(R.id.botoVerd).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt( null, getResources().getColor(R.color.colorBotoVerd));
                    }
                }
        );

        findViewById(R.id.botoVermell).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt(null, getResources().getColor(R.color.colorBotoVermell));
                    }
                }
        );

        vista.setOnTouchListener(new View.OnTouchListener() {
                                     @Override
                                     public boolean onTouch(View v, MotionEvent event) {
                                         if (MotionEvent.ACTION_DOWN != event.getAction())
                                             return false;

                                         nouPunt(event, getResources().getColor(R.color.colorTouch));
                                         return true;
                                     }
                                 }
        );
    }



    private void nouPunt(MotionEvent event, int color) {

  //      GeneradorDePuntsEstatic.GeneradorDePunts g = GeneradorDePuntsEstatic.obtenirGeneradorDePunts(event);
        GeneradorDePuntsEstatic.GeneradorDePunts g = GeneradorDePuntsEstatic.GeneradorDePunts.obtenirGeneradorDePunts(event);
        Punt punt = g.nouPunt(vista,event,color);

        cjtDePunts.afegeixPunt(punt);  // Afegim al model
        vistaPunt.avisaVistaPunt(punt);  // Avisem a la vista
    }



        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void testIteracio(){
        CjtDePunts c = new CjtDePunts();

        c.afegeixPunt(new Punt(10,10, Color.BLUE,2));
        c.afegeixPunt(new Punt(1,1, Color.BLUE,2));
        c.afegeixPunt(new Punt(13,31, Color.BLUE,1));


        for(Punt p: c) {
            Log.i(TAG, p.toString());
        }
    }





}
