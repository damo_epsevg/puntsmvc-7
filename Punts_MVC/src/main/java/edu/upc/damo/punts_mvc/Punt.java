/**
 * Created by Josep M on 30/09/13.
 */

package edu.upc.damo.punts_mvc;

import android.app.Application;

public class Punt {
    private final float x,y;
    private final int color;
    private final int diametre;

    public Punt(float x, float y, int color, int diametre)
    {this.x=x;
     this.y=y;
     this.color=color;
     this.diametre=diametre;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public int getColor() {
        return color;
    }

    public int getDiametre() {
        return diametre;
    }

    @Override
    public String toString()
    {return String.valueOf(x)+" "+String.valueOf(y);}
}
